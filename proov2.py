#!/usr/bin/env python3

# 95% gemini kokkukirjutatud

import tkinter as tk
from tkinter import ttk

def nupp_vajutatud(nupp):
    nupp.vajutuste_arv += 1
    nupp.config(text=f"Nupp {nupp.rida+1},{nupp.veerg+1}\n Vajutatud: {nupp.vajutuste_arv}")

def alusta_mängu():
    mängija1_tüüp = mängija1_combobox.get()
    mängija2_tüüp = mängija2_combobox.get()
    alusta_nupp.config(text=f"Mäng käib:\n1. mängija on {mängija1_tüüp}\n2. mängija on {mängija2_tüüp}\n\nAlustame uut mängu?")
    print(f"Mäng käib! 1. mängija on {mängija1_tüüp}, 2. mängija on {mängija2_tüüp}")
    # Siia lisa kood, mis käivitab mängu vastavalt valikutele

def valik_muutus(event):
    print(f"Valik muutus! Uus väärtus: {event.widget.get()}") # event.widget viitab combobox'ile, millel sündmus toimus
    if event.widget == mängija1_combobox:
        print("Muutus 1. mängija valik")
    elif event.widget == mängija2_combobox:
        print("Muutus 2. mängija valik")

aken = tk.Tk()
aken.title("3x3 Nupud")

# Combobox'id
raam1 = ttk.LabelFrame(aken, text="1. mängija")
raam1.grid(row=0, column=0, columnspan=3, padx=5, pady=5, sticky="ew")

mängija1_valikud = ["Arvuti", "Inimene"]
mängija1_combobox = ttk.Combobox(raam1, values=mängija1_valikud, state="readonly")
mängija1_combobox.current(0)
mängija1_combobox.grid(row=0, column=0, padx=5, pady=5, sticky="ew")

raam2 = ttk.LabelFrame(aken, text="2. mängija")
raam2.grid(row=1, column=0, columnspan=3, padx=5, pady=5, sticky="ew")

mängija2_valikud = ["Arvuti", "Inimene"]
mängija2_combobox = ttk.Combobox(raam2, values=mängija2_valikud, state="readonly")
mängija2_combobox.current(0)
mängija2_combobox.grid(row=0, column=0, padx=5, pady=5, sticky="ew")

# Seome sündmuse funktsiooniga
mängija1_combobox.bind("<<ComboboxSelected>>", valik_muutus)
mängija2_combobox.bind("<<ComboboxSelected>>", valik_muutus)

# Nupud
for rida in range(3):
    for veerg in range(3):
        nupp = tk.Button(aken, text=f"Nupp {rida+1},{veerg+1}\n Vajutatud: 0")
        nupp.grid(row=rida+2, column=veerg, padx=5, pady=5)
        nupp.rida = rida
        nupp.veerg = veerg
        nupp.vajutuste_arv = 0
        nupp.config(command=lambda n=nupp: nupp_vajutatud(n))

# "Alusta mängu" nupp
alusta_nupp = tk.Button(aken, text="Alusta mängu", command=alusta_mängu)
alusta_nupp.grid(row=5, column=0, columnspan=3, padx=5, pady=5, sticky="ew")

aken.mainloop()

'''
Peamised muudatused ja selgitused:

alusta_mängu() funktsioon: See uus funktsioon võtab combobox'idest valitud väärtused.

mängija1_tüüp = mängija1_combobox.get(): See rida on võtmetähtsusega. get() meetod tagastab

stringina combobox'is praegu valitud väärtuse.
Sama tehakse ka teise combobox'i jaoks.
print() funktsioonid kuvavad valitud väärtused konsoolis. 
Siia peaksid lisama koodi, mis reaalselt mängu käivitab, 
kasutades saadud väärtusi (mängija1_tüüp ja mängija2_tüüp).

"Alusta mängu" nupp:
alusta_nupp = tk.Button(aken, text="Alusta mängu", command=alusta_mängu): 
Loome uue nupu, mille vajutamisel käivitatakse alusta_mängu() funktsioon.

alusta_nupp.grid(...): Paigutame nupu akna allossa.

Kuidas see töötab:

Kasutaja valib combobox'idest soovitud mängijate tüübid.
Kui kasutaja vajutab nuppu "Alusta mängu", käivitatakse alusta_mängu() funktsioon.
Funktsioon kasutab get() meetodit, et saada kätte valitud väärtused mõlemast combobox'ist.
Valitud väärtused prinditakse konsooli ja neid saab kasutada mängu loogika käivitamiseks.
Nüüd saad korrektselt kätte informatsiooni, mis on combobox'idest valitud, ja saad seda 
kasutada oma mängu loogikas. See on palju parem lähenemine kui pidevalt monitoorida 
combobox'i muutumist, kuna see hoiab koodi lihtsa ja arusaadavana. 
Kui sul on veel küsimusi, küsi julgelt!

Peamised muudatused ja selgitused:

valik_muutus(event) funktsioon: See funktsioon kutsutakse välja iga kord, kui combobox'is valik muutub.
event argument sisaldab infot sündmuse kohta.
event.widget viitab combobox'ile, millel sündmus toimus. Selle abil saame teada, millisest combobox'ist valik muutus.
event.widget.get() tagastab uue valitud väärtuse.
Lisatud on ka kontroll, et kuvada vastav teade, kas muutus 1. või 2. mängija valik.
bind meetod:
mängija1_combobox.bind("<<ComboboxSelected>>", valik_muutus): See rida seob valik_muutus 
funktsiooni sündmusega <<ComboboxSelected>> esimesel combobox'il. Sama tehakse ka teise 
combobox'i jaoks. <<ComboboxSelected>> on spetsiifiline sündmus, mis genereeritakse iga kord, 
kui combobox'is tehakse uus valik.
Kuidas see töötab:

Kui kasutaja teeb valiku ühes combobox'idest, genereeritakse sündmus <<ComboboxSelected>>.
bind meetodi abil on see sündmus seotud valik_muutus funktsiooniga, mis seetõttu automaatselt välja kutsutakse.
valik_muutus funktsioon saab event objekti kaudu infot sündmuse kohta, 
sh uue valitud väärtuse ja combobox'i, millel sündmus toimus.
Funktsioon prindib konsooli vastava teate ja uue väärtuse.
See meetod on palju parem kui pidevalt kontrollida combobox'i väärtust, kuna see on 
sündmuspõhine ja reageerib ainult siis, kui valik tegelikult muutub. 
See on efektiivsem ja hoiab koodi puhtamana. Nüüd saad reageerida kohe, 
kui kasutaja muudab valikut, ja vastavalt sellele toimida.
'''