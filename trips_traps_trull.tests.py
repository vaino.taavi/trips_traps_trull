#!/usr/bin/python3

import trips_traps_trull


ttt = trips_traps_trull.TRIPSTRAPSTRULL()


def test_küsi_käiku_inimeselt():
    ttt.tee_tühi_väljak()
    ttt.käi_lahtrisse(1)
    ttt.n += 1
    ttt.käi_lahtrisse(2)
    ttt.n += 1
    ttt.käi_lahtrisse(3)
    ttt.n += 1
    ttt.käi_lahtrisse(4)
    ttt.näita_väljakut()
    ttt.n += 1
    idx = ttt.küsi_käiku_inimeselt()
    ttt.käi_lahtrisse(idx)
    ttt.näita_väljakut()


def test_leia_kõik_vabad_lahtrid():
    ttt.tee_tühi_väljak()
    assert ttt.leia_kõik_vabad_lahtrid() == [1, 2, 3, 4, 5, 6, 7, 8, 9]

    ttt.käi_lahtrisse(1)
    assert ttt.leia_kõik_vabad_lahtrid() == [2, 3, 4, 5, 6, 7, 8, 9]

    ttt.n += 1
    ttt.käi_lahtrisse(9)
    assert ttt.leia_kõik_vabad_lahtrid() == [2, 3, 4, 5, 6, 7, 8]
    pass


def testi_käi_lahtrisse():
    ttt.tee_tühi_väljak()
    assert ttt.käi_lahtrisse(3) == True
    ttt.näita_väljakut()
    ttt.n += 1
    assert ttt.käi_lahtrisse(9) == True
    ttt.näita_väljakut()
    ttt.n += 1
    assert ttt.käi_lahtrisse(9) == False
    ttt.näita_väljakut()


def test_tee_juhuslik_käik():
    ttt.tee_tühi_väljak()
    idx = ttt.tee_juhuslik_käik()
    pass


def test_kas_vaba():
    ttt.tee_tühi_väljak()
    ttt.käi_lahtrisse(3)
    ttt.n += 1
    ttt.käi_lahtrisse(9)
    assert ttt.kas_vaba(3) == False
    assert ttt.kas_vaba(9) == False
    assert ttt.kas_vaba(1) == True
    assert ttt.kas_vaba(2) == True
    assert ttt.kas_vaba(4) == True
    assert ttt.kas_vaba(5) == True
    assert ttt.kas_vaba(6) == True
    assert ttt.kas_vaba(7) == True
    assert ttt.kas_vaba(8) == True


def test_keegi_võitis():
    ttt.tee_tühi_väljak()
    assert ttt.kas_keegi_võitis() == False
    ttt.käi_lahtrisse(1)
    ttt.käi_lahtrisse(2)
    ttt.käi_lahtrisse(3)
    assert ttt.kas_keegi_võitis() == True
    ttt.käi_lahtrisse(1)
    ttt.käi_lahtrisse(4)
    ttt.käi_lahtrisse(7)
    assert ttt.kas_keegi_võitis() == True
    ttt.käi_lahtrisse(1)
    ttt.käi_lahtrisse(5)
    ttt.käi_lahtrisse(9)
    assert ttt.kas_keegi_võitis() == True
    ttt.tee_tühi_väljak()
    ttt.n += 1
    ttt.käi_lahtrisse(2)
    ttt.käi_lahtrisse(5)
    ttt.käi_lahtrisse(8)
    assert ttt.kas_keegi_võitis() == True


def test_kas_mina_saan_1_käiguga_võita():
    ttt.tee_tühi_väljak()
    ttt.käi_lahtrisse(2)
    ttt.käi_lahtrisse(1)
    ttt.näita_väljakut()
    assert ttt.kas_mina_saan_1_käiguga_võita() == 3
    assert ttt.kas_vastane_saab_1_käiguga_võita() == 0


def test_kes_mis_märgiga_mängib():
    ttt.tee_tühi_väljak()
    assert ttt.n == 1
    minumärk, vastasemärk = ttt.kes_mis_märgiga_mängib()
    assert minumärk == ttt.Xmärk
    assert vastasemärk == ttt.Omärk

    ttt.n = 2
    minumärk, vastasemärk = ttt.kes_mis_märgiga_mängib()
    assert minumärk == ttt.Omärk
    assert vastasemärk == ttt.Xmärk


if __name__ == "__main__":
    test_tee_juhuslik_käik()
    testi_käi_lahtrisse()
    test_leia_kõik_vabad_lahtrid()
    test_kas_vaba()
    test_küsi_käiku_inimeselt()
    test_keegi_võitis()
    test_kas_mina_saan_1_käiguga_võita()
    test_kes_mis_märgiga_mängib()
