#!/usr/bin/python3
'''
# terminali aknas
# alustad sellest (tõmbad endale teiste muudatused)
$ git pull # tõmbab versioonihoidlast uusima versiooni

# lõpetad sellega (salvestad oma muutused versioonihoidlas)
$ git status # näitab milleiseid faile oled muutnud
$ git add MUUDETUD-FAILI-NIMI
$ git commit -m "KOMMENTAAR MUUDATUSTE KOHTA"
$ git push # lisab versioonihoidlasse (internetis)


'''
import random
import sys

class TRIPSTRAPSTRULL:
    Xvärv   = '\u001b[35m' # magenta
    Ovärv   = '\u001b[36m' # cyan
    muuvärv = '\u001b[32m' # green

    Xmärk = f'{Xvärv}X{muuvärv}'
    Omärk = f'{Ovärv}O{muuvärv}'

    väljak = [] # mänguvälja

    kolmikud = [ # kõikvõimalikud read/veerud/diagonaalid
        # k[0]   k[1]   k[2]    tsüklis k=[k[0],k[1],k[2]]
        # read
        [ [0,0], [0,1], [0,2]], # 1. rida     
        [ [1,0], [1,1], [1,2]], # 2. rida
        [ [2,0], [2,1], [2,2]], # 3. rida
        # veerud
        [ [0,0], [1,0], [2,0]], # 1. veerg
        [ [0,1], [1,1], [2,1]], # 2. veerg
        [ [0,2], [1,2], [2,2]], # 3. veerg
        # diagonaalid
        [ [0,0], [1,1], [2,2]], # \ diagonaal
        [ [2,0], [1,1], [0,2]]  # / diagonaal                   
    ]   #  r v    r v    r v

    def test_kas_on_vabu_käike(self):
        self.väljak = [[1, 2, 3],[4, 5, 6],[7, 8, 9]] 
        tulemus =  self.kas_on_vabu_käike()

        self.väljak = [[self.Xmärk, 2, 3],[4, 5, 6],[7, 8, 9]] 
        tulemus =  self.kas_on_vabu_käike()

        self.väljak = [[1, self.Xmärk, 3],[4, 5, 6],[7, 8, 9]] 
        tulemus =  self.kas_on_vabu_käike()

        self.väljak = [[self.Xmärk, self.Xmärk, self.Xmärk],[self.Xmärk, self.Xmärk, self.Xmärk],[self.Xmärk, self.Xmärk, self.Xmärk]] 
        tulemus =  self.kas_on_vabu_käike()

        self.väljak = [[self.Xmärk, self.Xmärk, self.Xmärk],[self.Xmärk, self.Xmärk, self.Xmärk],[self.Xmärk, self.Xmärk, 9]] 
        tulemus =  self.kas_on_vabu_käike()

    def kas_on_vabu_käike_0(self): # ilma tsüklita versioon.
        if type(self.väljak[0][0]) is int:
            return True
        if type(self.väljak[0][1]) is int:
            return True
        if type(self.väljak[0][2]) is int:
            return True
        
        if type(self.väljak[1][0]) is int:
            return True
        if type(self.väljak[1][1]) is int:
            return True
        if type(self.väljak[1][2]) is int:
            return True
        
        if type(self.väljak[2][0]) is int:
            return True
        if type(self.väljak[2][1]) is int:
            return True
        if type(self.väljak[2][2]) is int:
            return True
        
        return False
        
    def kas_on_vabu_käike_1(self): # tsükliga versioon 1.0
        for x in range(3):
            if type(self.väljak[0][x]) is int:
                return True
  
        for x in range(3):
            if type(self.väljak[1][x]) is int:
                return True
            
        for x in range(3):
            if type(self.väljak[2][x]) is int:
                return True 
        return False         

    def kas_on_vabu_käike(self, teade=True): # tsükliga versioon 2.0
        for y in range(3):
            for x in range(3):
                if type(self.väljak[y][x]) is int:
                    return True
        if teade:
            print("Mäng jäi viiki!\n")
        return False
        
        
    def init_väljak(self):
        # intisialiseerime mängväljakut esitava tabeli lahtrinumbritega
        self.väljak = [[1, 2, 3],[4, 5, 6],[7, 8, 9]] 

    def näita_väljakut(self):
        print(f'{self.muuvärv}{self.väljak[0][0]}|{self.väljak[0][1]}|{self.väljak[0][2]}')
        print('-+-+-')
        print(f'{self.väljak[1][0]}|{self.väljak[1][1]}|{self.väljak[1][2]}')
        print('-+-+-')
        print(f'{self.väljak[2][0]}|{self.väljak[2][1]}|{self.väljak[2][2]}')

    '''
    def küsikäikuklaviatuurilt(self, n):
        rida_ja_veerg = { # lahtrinumber rea- ja veerunumbriks self.väljak tabelis
                1: [0,0], 2: [0,1], 3: [0,2], 
                4: [1,0], 5: [1,1], 6: [1,2], 
                7: [2,0], 8: [2,1], 9: [2,2]
                }        
        käija = self.Xmärk if n%2 ==0 else self.Omärk # paarisarvulised käigud teeb 'X', paaritud 'O'
        while True:
            print(f'Käib {käija}. Kuhu käid?')
            try:
                l = int(input())
            except:
                print("vigane sisend")
                continue
            if l >= 1 and l <= 9 :
                rida, veerg = rida_ja_veerg[l] # oli vahemikus 1-9
                if isinstance(self.väljak[rida][veerg], int) == True:
                    return rida, veerg # oli vaba, seega käiguks sobilik rida ja veerg
            print('Sinna ei saa käia!')
    '''
    def küsikäikuklaviatuurilt(self, n):
        lahtrinumber_rea_ja_veerunumbriks = { # lahtrinumber rea- ja veerunumbriks self.väljak tabelis
                1: [0,0], 2: [0,1], 3: [0,2], 
                4: [1,0], 5: [1,1], 6: [1,2], 
                7: [2,0], 8: [2,1], 9: [2,2]
                }        
        käija = self.Xmärk if n%2 ==0 else self.Omärk # paarisarvulised käigud teeb 'X', paaritud 'O'
        while True:
            print(f'Käib {käija}. Kuhu käid?')
            try:
                l = int(input())
                reanr, veerunr = lahtrinumber_rea_ja_veerunumbriks[l]
                if not (type(self.väljak[reanr][veerunr]) is int):
                    raise TypeError("Pole vaba lahter, sinna on juba käidud")
            except:
                print("Sinna ei saa käia!")
                continue # küsime uuesti
            return reanr, veerunr # oli vaba, seega käiguks sobilik rida ja veerg

    def käi(self, n, reanr, veernr):
        if n%2 ==0:
            self.väljak[reanr][veernr] = self.Xmärk
        else:
            self.väljak[reanr][veernr] = self.Omärk

    def kas_keegi_on_võitnud_v0(self): # ilma tsükliteta versioon
        """Kontrollime kas keegi on võitnud

        Ilma tsükliteta versioon

        Returns:
            bool: True: Keegi on juba võitnud + vastav teade ekraanile. False: keegi pole veel võitnud
        """
        # kontrollime ridasid
        #1. rida
        if (self.väljak[0][0] == self.väljak[0][1]) and (self.väljak[0][0] == self.väljak[0][2]): # rida 1
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True # reas 1 on 3 sama märki
        # 2. rida
        if (self.väljak[1][0] == self.väljak[1][1]) and (self.väljak[1][0] == self.väljak[1][2]): # rida 2
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True # reas 2 on 3 sama märki
        
        if (self.väljak[2][0] == self.väljak[2][1]) and (self.väljak[2][0] == self.väljak[2][2]): # rida 3
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True # reas 3 on 3 sama märki
        # 3. rida

        # kontrollime veerge
        # 1. veerg
        if (self.väljak[0][0] == self.väljak[1][0]) and (self.väljak[0][0] == self.väljak[2][0]): # veerg 1
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True # veerus 1 on 3 sama märki
        # 2. veerg
        if (self.väljak[0][1] == self.väljak[1][1]) and (self.väljak[2][1] == self.väljak[0][1]): # veerg 2
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True # veerus 2 on 3 sama märki
        # 3. veerg
        if (self.väljak[2][2] == self.väljak[1][2]) and (self.väljak[1][2] == self.väljak[2][2]): # veerg 3
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True # veerus 3 on 3 sama märki

        # kontrollime diagonaale
        # \ diagonaal
        if (self.väljak[0][0] == self.väljak[1][1]) and (self.väljak[1][1] == self.väljak[2][2]): # veerg 3
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True 
        
        if (self.väljak[0][2] == self.väljak[1][1]) and (self.väljak[1][1] == self.väljak[2][0]): # veerg 3
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True 

        return False # pole 3 sama märki reas, veerus või diagonaalis

    def kas_keegi_on_võitnud(self): #  2 tsükliga versioon
        """Kontrollime kas keegi on võitnud

        2 tsükliga versioon

        Returns:
            bool: True: Keegi on juba võitnud + vastav teade ekraanile. False: keegi pole veel võitnud
        """
        # read
        for r in range(3): # 3 reas
            if (self.väljak[r][0] == self.väljak[r][1]) and (self.väljak[r][0] == self.väljak[r][2]): # rida r
                print(f'{self.väljak[r][0]} võitis mängu!\n')
                return True  # reas r on 3 sama märki
            
        # veerud
        for v in range(3): # 3 veerus
            if (self.väljak[0][v] == self.väljak[1][v]) and (self.väljak[2][v] == self.väljak[1][v]): # veerg v
                print(f'{self.väljak[0][v]} võitis mängu!\n')
                return True # veerus v on 3 sama märki

        # diagonaalid
        if self.väljak[0][0] == self.väljak[1][1] and self.väljak[0][0] == self.väljak[2][2]: # diagonaal \
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True # diagonaalis \ on 3 sama märki
        if self.väljak[0][2] == self.väljak[1][1] and self.väljak[0][2] == self.väljak[2][0]: # diagonaal /
            print(f'{self.väljak[0][2]} võitis mängu!\n')
            return True # diagonaalis / on 3 sama märki
        
        return False # pole 3 sama märki reas, veerus või diagonaalis
    
    def kas_keegi_on_võitnud_v1a(self): #  1 tsükliga versioon
        """Kontrollime kas keegi on võitnud

        1 tsükkel + diagonaalide kontroll

        Returns:
            bool: True: Keegi on juba võitnud + vastav teade ekraanile. False: keegi pole veel võitnud
        """
        # read ja veerud
        for x in range(3):
            if (self.väljak[x][0] == self.väljak[x][1]) and (self.väljak[x][0] == self.väljak[x][2]): # rida x
                print(f'{self.väljak[x][0]} võitis mängu!\n')
                return True  # reas x on 3 sama märki
            
            if (self.väljak[0][x] == self.väljak[1][x]) and (self.väljak[2][x] == self.väljak[1][x]): # veerg x
                print(f'{self.väljak[0][x]} võitis mängu!\n')
                return True # veerus x on 3 sama märki

        # diagonaalid
        if self.väljak[0][0] == self.väljak[1][1] and self.väljak[0][0] == self.väljak[2][2]: # diagonaal \
            print(f'{self.väljak[0][0]} võitis mängu!\n')
            return True # diagonaalis \ on 3 sama märki
        if self.väljak[0][2] == self.väljak[1][1] and self.väljak[0][2] == self.väljak[2][0]: # diagonaal /
            print(f'{self.väljak[0][2]} võitis mängu!\n')
            return True # diagonaalis / on 3 sama märki
        
        return False # pole 3 sama märki reas, veerus või diagonaalis

    def kas_keegi_on_võitnud_v3(self):
        """Kontrollime kas keegi on võitnud

        Returns:
            bool: True: keegi on võitnud + teade. False: keegi pole veel võitnud.
        """
        
        # käime ühte otsa: 3==vaba ja 1 ==märk ja 2==märk -> 3
        for k in self.kolmikud:
            lahter1rida, lahter1veerg = k[0] # lahter1 -> lahter1rida, lahter1veerg = k[0][0], k[0][1]
            lahter2rida, lahter2veerg = k[1] # lahter2
            lahter3rida, lahter3veerg = k[2] # lahter3
            if self.väljak[lahter3rida][lahter3veerg] == self.väljak[lahter1rida][lahter1veerg ] and \
                    self.väljak[lahter1rida][lahter1veerg ]== self.väljak[lahter2rida][lahter2veerg]:
                print(f"{self.väljak[lahter3rida][lahter3veerg]} Võitis mängu")
                return True
        return False

    def test_kas_saab_1_käiguga_võita(self):
        if False: # kontrolli ridasid
            for r in range(3):
                self.init_väljak()
                self.käi(0, r, 0)
                self.käi(0, r, 1)
                self.näita_väljakut()
                rida, veerg = self.kas_saab_1_käiguga_võita(self.Xmärk)
                if rida != r and veerg != 2:
                    print("JAMA")
                else:
                    print("ok")
            for r in range(3):
                self.init_väljak()
                self.käi(0, r, 0)
                self.käi(0, r, 2)
                self.näita_väljakut()
                rida, veerg = self.kas_saab_1_käiguga_võita(self.Xmärk)
                if rida != r and veerg != 1:
                    print("JAMA")
                else:
                    print("ok")
            for r in range(3):
                self.init_väljak()
                self.käi(0, r, 1)
                self.käi(0, r, 2)
                self.näita_väljakut()
                rida, veerg = self.kas_saab_1_käiguga_võita(self.Xmärk)
                if rida != r and veerg != 0:
                    print("JAMA")
                else:
                    print("ok")
        if False: # kontrolli veergepidi
            for v in range(3):
                self.init_väljak()
                self.käi(0, 0, v)
                self.käi(0, 1, v)
                self.näita_väljakut()
                rida, veerg = self.kas_saab_1_käiguga_võita(self.Xmärk)
                if rida != 2 and veerg != v:
                    print("JAMA")
                else:
                    print("ok")
            for v in range(3):
                self.init_väljak()
                self.käi(0, 0, v)
                self.käi(0, 2, v)
                self.näita_väljakut()
                rida, veerg = self.kas_saab_1_käiguga_võita(self.Xmärk)
                if rida != 1 and veerg != v:
                    print("JAMA")
                else:
                    print("ok")
                
            for v in range(3):
                self.init_väljak()
                self.käi(0, 1, v)
                self.käi(0, 2, v)
                self.näita_väljakut()
                rida, veerg = self.kas_saab_1_käiguga_võita(self.Xmärk)
                if rida != 0 and veerg != v:
                    print("JAMA")
                else:
                    print("ok")
        if False:
            self.init_väljak()
            self.käi(0, 0, 2)
            self.käi(0, 1, 1)
            self.näita_väljakut()
            rida, veerg = self.kas_saab_1_käiguga_võita(self.Xmärk)
            if rida != 2 and veerg != 0:
                print("JAMA")
            else:
                print("ok")
        pass
        
    def kas_saab_1_käiguga_võita_uus(self, märk):
        """Vaatab kas saab 1 käiguga võita

        Returns:
            int, int: reanr, veerunr kui sinna ritta ja veergu käies saab võita
            None, None: pole sellist käiku, millega saaks võita
        """
        for k in self.kolmikud:
            lahter1rida, lahter1veerg = k[0] # lahter1 -> lahter1rida, lahter1veerg = k[0][0], k[0][1]
            lahter2rida, lahter2veerg = k[1] # lahter2
            lahter3rida, lahter3veerg = k[2] # lahter3
            # käime ühte otsa: 3==vaba ja 1 ==märk ja 2==märk -> 3
            if type(self.väljak[lahter3rida][lahter3veerg]) is int and \
                                self.väljak[lahter1rida][lahter1veerg]==märk and \
                                self.väljak[lahter2rida][lahter2veerg]==märk:
                return lahter3rida, lahter3veerg  #v[2]
            
            # käime keskele: 2==vaba ja 1==märk ja 3==märk -> 2
            if type(self.väljak[lahter2rida][lahter2veerg]) is int and \
                                self.väljak[lahter1rida][lahter1veerg]==märk and \
                                self.väljak[lahter3rida][lahter3veerg]==märk:
                return lahter2rida, lahter2veerg  #v[2]
            
            # käime teise otsa: 1==vaba ja 2==märk ja 3==märk -> 1
            if type(self.väljak[lahter1rida][lahter1veerg]) is int and \
                                self.väljak[lahter3rida][lahter3veerg]==märk and \
                                self.väljak[lahter2rida][lahter2veerg]==märk:
                return lahter1rida, lahter1veerg  #v[2]
        return None, None
            
          
    def kas_saab_1_käiguga_võita(self, märk):#Täiustame

        # Kontrollime ridasid 
        #   märk|märk|arvuti käib siia      
        for reanr in range(3):
            if (self.väljak[reanr][0] == märk) and (self.väljak[reanr][1] == märk) and isinstance(self.väljak[reanr][2], int):
                return reanr, 2   
        #   märk|arvuti käib siia|märk 
        for reanr in range(3):
            if (self.väljak[reanr][0] == märk) and (self.väljak[reanr][2] == märk) and isinstance(self.väljak[reanr][1], int):
                return reanr, 1
        #   arvuti käib siia|märk|märk
        for reanr in range(3):
            if (self.väljak[reanr][1] == märk) and (self.väljak[reanr][2] == märk) and isinstance(self.väljak[reanr][0], int):
                return reanr, 0

        #Kontrollime veergusid
        #märk
        #märk
        #arvuti käib siia
        for veerunr in range(3):
            if (self.väljak[0][veerunr] == märk) and (self.väljak[1][veerunr] == märk) and isinstance(self.väljak[2][veerunr], int):
                return 2, veerunr
        
        #arvuti käib siia   
        #märk
        #märk
        for veerunr in range(3):
            if (self.väljak[1][veerunr] == märk) and (self.väljak[2][veerunr] == märk and isinstance(self.väljak[0][veerunr], int)):
                return 0, veerunr
        
        #märk
        #arvuti käib siia
        #märk
        for veerunr in range(3):
            if (self.väljak[0][veerunr] == märk) and (self.väljak[2][veerunr] == märk and isinstance(self.väljak[1][veerunr], int)):
                return 1, veerunr
        
        # Kontrollime diagonaale 1
        if (self.väljak[0][2] == märk) and (self.väljak[1][1] == märk) and isinstance(self.väljak[2][0], int):
            return 2, 0
        
        if (self.väljak[2][0] == märk) and (self.väljak[0][2] == märk) and isinstance(self.väljak[1][1], int):
            return 1, 1
        
        if (self.väljak[2][0] == märk) and (self.väljak[1][1] == märk) and isinstance(self.väljak[0][2], int):
            return 0, 2
        
        # Kontrollime diagonaale 2
        if (self.väljak[0][0] == märk) and (self.väljak[1][1] == märk) and isinstance(self.väljak[2][2], int):
            return 2, 2
        
        if (self.väljak[0][0] == märk) and (self.väljak[2][2] == märk) and isinstance(self.väljak[1][1], int):
            return 1, 1
        
        if (self.väljak[2][2] == märk) and (self.väljak[1][1] == märk) and isinstance(self.väljak[0][0], int):
            return 0, 0
        
        return None, None

    def arvutipakutudkäik(self, n, arvutimärk, vastasemärk):
        reanr = None
        veerunr = None
        # kontrollime, kui arvuti saab 1 käiguga võita, siis käi sinna
        rida, veerg = self.kas_saab_1_käiguga_võita_uus(arvutimärk)
        if rida is not None:
            return rida, veerg
        # kontrollime, kui vastane saab 1 käiguga võita, siis käi sinna ette
        rida, veerg = self.kas_saab_1_käiguga_võita_uus(vastasemärk)
        if rida is not None:
            return rida, veerg
        
        # Kui on "O" 1. käik (n==1) ja keskmine ruut on vaba, käi sinna
        if n == 1 and isinstance(self.väljak[1][1], int):
            return 1, 1 # rikume vastase potentsiaalse kahvli ehitamise ära

        # Kui on "O" 2. käik (n==3) käi 01, 10, 12 või 21
        if n == 3 and self.väljak[1][1] == arvutimärk:
            variandid = [[0, 1],[1, 0],[1, 2],[2, 1]]
            for v in variandid:
                if isinstance(self.väljak[v[0]][v[1]], int):
                    return v[0], v[1]
        # Kahvli 3. haru tegemine
        # 3. haru: loodes ja kagus arvutimärk olemas
        if self.väljak[0][0] == arvutimärk and self.väljak[2][2] == arvutimärk:
            if isinstance(self.väljak[2][0], int): # edelas vaba, pane arvutimärk sinna
                return 2, 0
            if isinstance(self.väljak[0][2], int): # kirdes vaba, pane arvutimärk sinna
                return 0, 2
        # 3. haru: kirdes ja edelas  arvutimärk olemas
        elif self.väljak[2][0] == arvutimärk and self.väljak[0][2] == arvutimärk:
            if isinstance(self.väljak[0][0], int): # loodes vaba, pane arvutimärk sinna
                return 0, 0
            if isinstance(self.väljak[2][2], int): # kagus vaba, pane arvutimärk sinna
                return 2, 2
        # Kahvli 2. haru tegemine  
        if self.väljak[0][0] == arvutimärk and isinstance(self.väljak[2][2], int):
            return 2, 2 
        elif self.väljak[0][2] == arvutimärk and isinstance(self.väljak[2][0], int):
            return 2, 0 
        elif self.väljak[2][2] == arvutimärk and isinstance(self.väljak[0][0], int):
            return 0, 0  
        elif self.väljak[2][0] == arvutimärk and isinstance(self.väljak[0][2], int):
            return 0, 2
        # Kahvli 1. haru tegemine
        if random.randrange(2) == 1:
            if isinstance(self.väljak[0][0], int) and isinstance(self.väljak[2][2], int):
                diagonaali_otsad = [[0,0], [2,2]]
                rida, veerg = diagonaali_otsad[random.randrange(2)]
                return rida, veerg
        else:
            if isinstance(self.väljak[2][0], int) and isinstance(self.väljak[0][2], int):
                diagonaali_otsad = [[2,0], [0,2]]
                rida, veerg = diagonaali_otsad[random.randrange(2)]
                return rida, veerg

        # arvuti käib esimese vaba koha peale
        # TODO selle võiks edasiarendada juhuslikuks käiguks
        for r in range(3):
            for v in range(3):
                if self.väljak[r][v] != arvutimärk and self.väljak[r][v] != vastasemärk:
                    return r, v

        print("\nolen valesti mõelnud\n") # seda ei tohiks juhtuda
        exit()

    def inimene_vs_inimene(self):
        print("\nInimene vs inimene\n")
        
        self.init_väljak()
        self.näita_väljakut()
        n = 0
        while True:
            reanr, veerunr = self.küsikäikuklaviatuurilt(n)
            ttt.käi(n, reanr, veerunr)
            self.näita_väljakut()
            keegi_võitis = self.kas_keegi_on_võitnud_v3()
            if keegi_võitis is True:
                break
            if self.kas_on_vabu_käike() == False:
                break
            n += 1

    def arvuti_alustab(self):
        print("\nInimene vs arvuti (arvuti alustab)\n")
        self.init_väljak()
        n = 0
        while True:
            # arvuti käik
            reanr, veerunr = self.arvutipakutudkäik(n, self.Xmärk, self.Omärk)
            print(f"Arvuti käib {self.väljak[reanr][veerunr]}")
            ttt.käi(n, reanr, veerunr)
            self.näita_väljakut()
            keegi_võitis = self.kas_keegi_on_võitnud_v3()
            if keegi_võitis is True:
                break
            if self.kas_on_vabu_käike() is False:
                break
            n +=1
            # inimese käik

            reanr, veerunr = self.küsikäikuklaviatuurilt(n)
            ttt.käi(n, reanr, veerunr)
            self.näita_väljakut()
            keegi_võitis = self.kas_keegi_on_võitnud_v3()
            if keegi_võitis is True:
                break
            n += 1

    def inimene_alustab(self):
        print('\nArvuti vs inimene (inimene alustab)\n')
        self.init_väljak()
        n = 0
        self.näita_väljakut()
        while True:
            # inimese käik
            reanr, veerunr = self.küsikäikuklaviatuurilt(n)
            ttt.käi(n, reanr, veerunr)
            self.näita_väljakut()
            keegi_võitis = self.kas_keegi_on_võitnud_v3()
            if keegi_võitis is True:
                break
            if self.kas_on_vabu_käike() is False:
                break
            n +=1
            # arvuti käik
            reanr, veerunr = self.arvutipakutudkäik(n, self.Omärk, self.Xmärk)
            print(f"Arvuti käib {self.väljak[reanr][veerunr]}")
            ttt.käi(n, reanr, veerunr)
            self.näita_väljakut()
            keegi_võitis = self.kas_keegi_on_võitnud_v3()
            if keegi_võitis is True:
                break  
            n += 1

    def arvuti_vs_arvuti(self):
        print("\nArvuti vs arvuti\n")
    
        self.init_väljak()
        n = 0
        while True:
            # 1. mängija (see kes alustas, käib 'X' märke)
            reanr, veerunr = self.arvutipakutudkäik(n, self.Xmärk, self.Omärk)
            print(f"Arvuti 'X' käib {self.väljak[reanr][veerunr]}")
            ttt.käi(n, reanr, veerunr)
            self.näita_väljakut()
            keegi_võitis = self.kas_keegi_on_võitnud_v3()
            if keegi_võitis is True:
                break 
            if self.kas_on_vabu_käike() is False:
                print('Mäng jäi viiki!!!!!!!!!!!')
                break
            print('Jätkamiseks vajuta ENTER')
            input()
            n += 1
            # 2. mängija (see kes ei alustanud, käib '0' märke)
            reanr, veerunr = self.arvutipakutudkäik(n, self.Omärk, self.Xmärk)
            print(f"Arvuti 'O' käib {self.väljak[reanr][veerunr]}")
            ttt.käi(n, reanr, veerunr)
            self.näita_väljakut()
            keegi_võitis = self.kas_keegi_on_võitnud_v3()
            if keegi_võitis is True:
                break
            print('Jätkamiseks vajuta ENTER')
            input()
            n += 1    

    def random_käik(self):
        masiiv = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        masiiv[random.randrange(9)]
        print(masiiv[random.randrange(9)])


    def arvuti_vs_arvuti_tsüklis(self):
        x = 0
        lõppes_viigiga = True
        while lõppes_viigiga: # tsükkel üle mängude
            x +=1
            sys.stdout.write(f'\r{x}  ') # mäng nr x
            self.init_väljak()
            n = 0
            while True:
                # 1. mängija (see kes alustas, käib 'X' märke)
                reanr, veerunr = self.arvutipakutudkäik(n, self.Xmärk, self.Omärk)
                ttt.käi(n, reanr, veerunr)
                keegi_võitis = self.kas_keegi_on_võitnud_v3()
                if keegi_võitis is True:
                    self.näita_väljakut()
                    lõppes_viigiga = False
                    break 
                if self.kas_on_vabu_käike(False) is False:
                    break
                n += 1
                # 2. mängija (see kes ei alustanud, käib '0' märke)
                reanr, veerunr = self.arvutipakutudkäik(n, self.Omärk, self.Xmärk)
                ttt.käi(n, reanr, veerunr)
                keegi_võitis = self.kas_keegi_on_võitnud_v3()
                if keegi_võitis is True:
                    self.näita_väljakut()
                    lõppes_viigiga = False
                    break
                n += 1   

    def juhuslik_käik(self):
        reanr, veerunr = None, None
        vabad_käigud = [] # massiiv vabadest käikudest [reanr, veerunr]
        #korja massiivi kokku kõik vabad käigud
        # vali vabade käikude hulgast juhuslikult 1

        return reanr, veerunr

if __name__ == '__main__':
    ttt = TRIPSTRAPSTRULL()
    ttt.arvuti_vs_arvuti_tsüklis()
    #ttt.test_kas_on_vabu_käike()
    #ttt.test_kas_saab_1_käiguga_võita()
    #ttt.random_käik()
    
    while True:
        print('1 Inimene vs inimene')
        print('2 Inimene alustab')
        print('3 Arvuti alustab')
        print('4 Arvuti vs arvuti')
        print('0 Aitab mängimisest')
        print('Valik?: ')
        #todo sisendi kontroll
        valik = int(input())
        if valik  == 1:
            ttt.inimene_vs_inimene()
        elif valik  == 2:
            ttt.inimene_alustab()
        elif valik == 3:
            ttt.arvuti_alustab()
        elif valik == 4:
            ttt.arvuti_vs_arvuti()
        elif valik == 0:
            break
        else:
            print('Vali 1, 2, 3, 4 või 0')
       